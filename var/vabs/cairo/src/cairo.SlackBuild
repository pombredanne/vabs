#!/bin/sh

# Copyright 2008, 2009, 2010, 2011  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NAME="cairo"
VERSION=${VERSION:-"1.12.14"}
BUILDNUM=${BUILDNUM:-"2"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"http://cairographics.org/releases/$NAME-$VERSION.tar.xz"}
MAKEDEPENDS=${MAKEDEPENDS:-"pixman xcb-util"}
#"http://cairographics.org/releases/cairo-1.12.4.tar.xz"

if [ "$NORUN" != 1 ]; then
# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

NUMJOBS=${NUMJOBS:-" -j6 "}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
  CONFIGURE_TRIPLET="i486-vector-linux"
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi
#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp

PKG=$TMP/package-cairo
rm -rf $PKG
mkdir -p $TMP $PKG

cd $TMP
rm -rf cairo-$VERSION
tar xvf $CWD/cairo-$VERSION.tar.?z* || exit 1
cd cairo-$VERSION
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

# Thanks to Ubuntu for this, but I found it at Gentoo's bugtracker
# http://bugs.gentoo.org/show_bug.cgi?id=336696
#zcat $CWD/cairo-1.10.0-buggy_gradients.patch.gz | patch -p1 || exit 1

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --sysconfdir=/etc \
  --disable-gtk-doc \
  --disable-quartz \
  --disable-static \
  --disable-win32 \
  --disable-trace \
  --enable-xlib \
  --enable-xcb \
  --enable-xlib-xcb \
  --enable-xcb-shm \
  --enable-freetype \
  --enable-ps \
  --enable-pdf \
  --enable-svg \
  --enable-tee \
  --enable-gobject \
  --enable-xml \
  --build=$CONFIGURE_TRIPLET || exit 1
# None of these are 'stable' yet...
#  --enable-qt \
#  --enable-gl \
#  --enable-drm \
#  --enable-xcb \
#  --enable-xlib-xcb \
#  --enable-xcb-drm \
#  --enable-drm-xr \
# Skipping this, because it causes a dependency on the specific
# version of binutils installed at compile time:
#  --enable-trace

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG

find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

mkdir -p $PKG/usr/doc/cairo-$VERSION
cp -a \
  AUTHORS BIBLIOGRAPHY BUGS CODING_STYLE COPYING* HACKING NEWS PORTING_GUIDE README RELEASING \
  $PKG/usr/doc/cairo-$VERSION
( cd $PKG/usr/doc/cairo-$VERSION ; ln -sf /usr/share/gtk-doc/html/cairo html )

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/slack-desc > $RELEASEDIR/slack-desc

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
fi
