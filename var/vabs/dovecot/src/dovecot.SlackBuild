#!/bin/bash

# Copyright 2006, Alan Hicks, Lizella, GA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM="dovecot"
NAME="$PRGNAM"
VERSION=${VERSION:-"2.2.5"}
ARCH=${ARCH:-"$(uname -m)"}
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSSION"}

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG="$TMP/package-$PRGNAM"
OUTPUT=${OUTPUT:-$CWD/..}
LINK=${LINK:-"http://www.dovecot.org/releases/$(echo $VERSION |cut -f1-2 -d.)/$NAME-$VERSION.tar.gz"}
NUMJOBS=${NUMJOBS:-"-j6"}

if [ "$NORUN" != 1 ]; then

if [ -f /var/log/packages/libiconv-* ]; then
	echo "libiconv is installed in this system.  This conflicts with $NAME"
	echo "libiconv will be removed in 30 seconds.  Press CTRL+C to stop"
	sleep 30
	removepkg libiconv || exit 1
	slapt-get -u
	slapt-get -y -i --reinstall glibc || exit 1
fi

# Bail if user isn't valid on your system
if ! grep -q ^dovecot: /etc/passwd ; then
  echo "  You must have a dovecot user to run this script."
  echo "    # groupadd -g 202 dovecot"
  echo "Adding group "
	groupadd -g 202 dovecot
  echo "    # useradd -d /dev/null -s /bin/false -u 202 -g 202 dovecot"
	echo "Adding user"
	useradd -d /dev/null -s /bin/false -u 202 -g 202 dovecot
  echo "  Or something similar."
#  exit 1
fi

set -e


#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------

# Download the source

for SRC in $(echo $LINK); do
	( cd $CWD
	wget -c --no-check-certificate $SRC
	)
done

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
tar xvf $CWD/$NAME-$VERSION.tar.gz
cd $NAME-$VERSION
chmod -R a-s,u+w,go+r-w .
chown -R root:root .

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --localstatedir=/var \
  --docdir=/usr/doc/$PRGNAM-$VERSION \
  --enable-header-install \
  --with-notify=inotify \
  --disable-ipv6 \
  --without-pam \
  --with-pop3d \
  --disable-static \
  --build=$CONFIGURE_TRIPLET || exit 1

make $NUMJOBS || exit 1
make install-strip DESTDIR=$PKG

set +e

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a AUTHORS COPYING COPYING.LGPL COPYING.MIT ChangeLog INSTALL NEWS \
   README TODO doc/* $PKG/usr/doc/$PRGNAM-$VERSION/
cat $CWD/dovecot.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/dovecot.SlackBuild
rm -rf $PKG/usr/share/doc

# Put the example files with the documentation
mv $PKG/etc/dovecot-ldap-example.conf $PKG/etc/dovecot-sql-example.conf \
   $PKG/usr/doc/$PRGNAM-$VERSION

# Add an init script
mkdir -p $PKG/etc/rc.d
install -m 0755 $CWD/rc.dovecot $PKG/etc/rc.d/rc.dovecot.new

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG

echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $OUTPUT $PKG
cat $PKG/install/slack-required >$OUTPUT/slack-required
cat $CWD/slack-desc > $OUTPUT/slack-desc
/sbin/makepkg -l y -c n $OUTPUT/$NAME-$VERSION-$ARCH-$BUILD.${PKGTYPE:-txz}

fi
